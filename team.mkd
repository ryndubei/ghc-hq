# The GHC Team

This page lists the members of the GHC Team.

If you would like to be a member of the team, and you are happy to undertake the responsibilities
outlined on the [GHC HQ home page](README.mkd), just
* make a PR to add your name to this table, and
* send an email to the [`ghc-devs` mailing list](https://mail.haskell.org/mailman/listinfo/ghc-devs) to introduce yourself.

If you don't want to be a member of the team, make a PR to remove yourself.


| Name                  | User name           | Primary areas of interest and expertise               |
| --------------------- | ------------------- | ----------------------------------------------------- |
| Adam Gundry           | @adamgundry         |                                                       |
| Alan Zimmerman        | @alanz              |  exact printing                                       |
| Alex D                | @nineonine          |                                                       |
| Alexis King           | @lexi.lambda        |                                                       |
| Andreas Klebinger     | @andreask           | Code Generation, RTS, Runtime Performance             |
| Andreas Schwab        | @schwab             |                                                       |
| Andrei Borzenkov      | @sand-witch         | parser, renamer, also want to do typechecker          |
| Bartłomiej Cieślar    | @barci2             |                                                       |
| Ben Gamari            | @bgamari            | RTS, Core optimisation, code generation, packaging    |
| Bryan Richter         | @chreekat           | Contributor workflow, gitlab.haskell.org, CI          |
| Cheng Shao            | @TerrorJack         | WebAssembly backend, Code Generation, RTS                                                      |
| David Feuer           | @treeowl            |                                                       |
| Duncan Coutts         | @dcoutts            |                                                       |
| Finley McIlwaine      | @FinleyMcIlwaine    | monitoring, profiling, haddock                        |
| Gergő Érdi            | @cactus             | Using (and abusing!) GHC as an API                    |
| Jaro Reinders         | @Noughtmare         | i386, rules, unlifted types                           |
| Jeff Young            | @doyougnu           | Runtime and compiler performance, backpack, javascript backend, modularity                                        |
| John Ericson          | @Ericson2314        |                                                       |
| Josh Meredith         | @JoshMeredith       |                                                       |
| Krzysztof Gogolewski  | @monoidal           |                                                       |
| Luite Stegeman        | @luite              |                                                       |
| Matt Pickering        | @mpickering         | infrastructure, monitoring, profiling, typed template haskell   |
| Matthew Craven        | @clyring            |                                                       |
| Melanie Brown         | @mixphix            |                                                       |
| Moritz Angermann      | @angerman           |                                                       |
| Rodrigo Mesquita      | @alt-romes          | linear types, code-gen, runtime-retargetability/toolchains, pattern-match checking |
| Ryan Scott            | @RyanGlScott        | deriving, Template Haskell                            |
| Sam Derbyshire        | @sheaf              | typechecker, renamer, primops, records, representation polymorphism, pattern-match checking                                                      |
| Sebastian Graf        | @sgraf812           | Core and STG analysis and optimisation, pattern-match checking, levity polymorphism, performance |
| Simon Peyton Jones    | @simonpj            | renamer, typechecker, simplifier                      |
| Sven Tennie           | @supersven          | ghc-heap, closure-related parts of RTS, native code generation backends |
| Sylvain Henry         | @hsyl20             | JS backend, performance, modularity, ghc-bignum       |
| Teo Camarasu          | @teo                |                                                       |
| Torsten Schmits       | @torsten.schmits    |Wired-in types, breakpoints, renamer, bytecode         |
| Vasily Sterekhov      | @ryndubei           |                                                       |
| Vladislav Zavialov    | @int-index          | parser, renamer                                       |
| Zubin                 | @wz1000             |                                                       |
| fendor                | @fendor             |                                                       |
| mitzuki, arata        | @aratamizuki        |                                                       |
